# Proticity Java Gradle Plugin
The Proticity Java Gradle Plugin helps minimize repetition across numerous Proticity Java projects by automatically
applying several settings that are commonly used by (or sane defaults for) each project. It is not intended for
widespread use outside of the Proticity project. For a complete suite of functionality in a single Gradle plugin, meant
for reuse by other developers and projects. see the
[Java-Modern Gradle Plugin](https://gitlab.com/proticity/cloud/java-modern-gradle-plugin).