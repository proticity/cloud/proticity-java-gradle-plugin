/*
 * SPDX-License-Identifier: Apache-2.0
 *
 * Copyright 2019 John Stewart.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.proticity.gradle.plugin.java

import groovy.transform.CompileStatic
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.plugins.quality.CheckstyleExtension
import org.gradle.plugins.ide.idea.model.IdeaModel
import org.kordamp.gradle.plugin.base.ProjectConfigurationExtension
import org.proticity.gradle.plugin.idea.IdeaPlusConfig
import org.proticity.gradle.plugin.idea.IdeaPlusExtension
import org.proticity.gradle.plugin.javamodern.JavaModernPlugin

/**
 * The plugin which preconfigures Java projects.
 */
@CompileStatic
class JavaPlugin implements Plugin<Project> {
    @Override
    void apply(Project project) {
        System.setProperty('build.scan.agree', 'yes')

        project.plugins.apply(JavaModernPlugin)

        def codeStyleConfig = JavaPlugin.getResourceAsStream('/idea-codestyle.xml')
        def checkstyleConfig = JavaPlugin.getResourceAsStream('/proticity-checkstyle.xml')

        // Setup standard CheckStyle configuration.
        project.extensions.findByType(CheckstyleExtension).with { CheckstyleExtension ext ->
            project.logger.debug('Finding bundled Proticity Checkstyle configuration.')
            ext.config = project.resources.text.fromString(checkstyleConfig.text)
        }

        // Preconfigure IDEA-Plus with a code style which matches the Checkstyle configuration.
        project.extensions.findByType(IdeaPlusExtension).with {
            config { IdeaPlusConfig ext ->
                ext.useProjectStyle = true
                ext.codeStyle = project.resources.text.fromString(codeStyleConfig.text)
            }
        }

        // Setup default project descriptions.
        project.extensions.findByType(ProjectConfigurationExtension).with {
            info {
                links {
                    website = 'https://www.proticity.org'
                    issueTracker = "https://gitlab.com/proticity/cloud/${project.name}/issues"
                    scm = "https://gitlab.com/proticity/cloud/${project.name}"
                }

                scm {
                    url = "https://gitlab.com/proticity/cloud/${project.name}"
                    connection = "https://gitlab.com/proticity/cloud/${project.name}.git"
                    developerConnection = "git@gitlab.com:proticity/cloud/${project.name}.git"
                }

                organization {
                    name = 'Proticity'
                    url = 'https://www.proticity.org'
                }
            }

            publishing {
                releasesRepository = 'ossrh-releases'
                snapshotsRepository = 'ossrh-snapshots'
                signing = true
            }
        }


    }
}